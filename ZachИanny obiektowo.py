import math
import sys

class Miasto:

    def __init__(self, x=0, y=0, nr=1, odzwiedzony =0):
        self.x = x
        self.y=y
        self.nr = nr
        self.odwiedzony = odzwiedzony

    #liczenie odleglosci
    def odleglosc(self, a):
        odleglosc = (self.x - a.x) * (self.x - a.x) + (self.y - a.y) * (self.y - a.y)
        gotowa = math.sqrt(odleglosc)
        return gotowa




plik = (input("Podaj nazwe pliku")+".txt")
# ile jest wierzcholkow
with open(plik) as f:
    first_line = f.readline()
    pierwsza = first_line.split()
    print("Ilosc miast do odwiedzenia to", pierwsza[0], "\n")
ilosc = int(pierwsza[0])

miasta = []
for i in range(ilosc):
    miasta.append(Miasto())



for klapa in range(int(pierwsza[0])):

    #dodaje kazdy nr, x i y kazdego miasta do 3 list oraz sprawdza czy kazda z tych wartosci jest liczbowa
    with open(plik, 'r') as f:
        for i, x in enumerate(f):
            if i >= 1:
                Linia = x.split()
                p = Linia[0].isdigit()
                l = Linia[1].isdigit()
                k = Linia[2].isdigit()
                if p+l+k is 3 or 0 or 1 or 2:
                    miasta[i-1].nr=float(Linia[0])
                    miasta[i - 1].x=float(Linia[1])
                    miasta[i - 1].y=float(Linia[2])

                else:
                    print("Błąd w pliku wejsciowym")
                    sys.exit(0)
ostateczna = 1000000
for g in range(ilosc):
    koncowa=0
    for l in range(ilosc):
        miasta[l].odwiedzony=0

    f = g
    a=0
    while(a<ilosc):
        znaleziony= False
        mini = 1000000
        ostatnie = g
        miasta[g].odwiedzony=1
        for k in range(ilosc):
            if miasta[k].odwiedzony==0:
                odl = miasta[g].odleglosc(miasta[k])
                #print("g", g, "k", k)
                #print(odl)
                if odl<mini:
                    mini = odl
                    #print(mini)
                    j = k
                    znaleziony = True

        g=j
        a=a+1
        koncowa=koncowa+mini


    powrot = miasta[f].odleglosc(miasta[ostatnie])
    if not znaleziony:
        koncowa = koncowa - 1000000

    if koncowa+powrot < ostateczna:
        ostateczna = koncowa + powrot

        poczatek = f

print(ostateczna, poczatek)
