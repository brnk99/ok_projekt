import random

import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

from ant_colony import ant_colony

from Mrowki import AntColony

nodes = [(random.randint(0, 100), random.randint(0, 100)) for _ in range(70)]


def generate_plot(iteration, connections, total):
    for x, y in nodes:
        plt.scatter(x, y)

    for connection in connections:
        line(connection[0], connection[1])
    plt.title("{} dystans: {}".format(iteration, total))
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.savefig("plots/"+str(iteration))
    plt.show()


def line(node, dnode):
    arrow = list(map(lambda x: x[1] - x[0], zip(nodes[node], nodes[dnode])))
    plt.arrow(*nodes[node], *arrow)


def distance(start, end):
    x_distance = abs(start[0] - end[0])
    y_distance = abs(start[1] - end[1])

    import math
    return math.sqrt(pow(x_distance, 2) + pow(y_distance, 2))


distances = []

for n in range(len(nodes)):
    lista = [[]] * len(nodes)
    distances.append(lista)

for a, node1 in enumerate(nodes):
    for b, node2 in enumerate(nodes):
        if a == b:
            distances[a][b] = np.inf
        else:
            distances[a][b] = np.math.sqrt(((node1[0] - node2[0]) ** 2) + ((node1[1] - node2[1]) ** 2))

ant_colony = ant_colony({i: node for i, node in enumerate(nodes)}, distance, alpha=1, beta=2, pheromone_evaporation_coefficient=0.87, ant_count=200)

for iteration in range(1000):

    ant_colony.iterate()

    path = []

    for i in range(len(ant_colony.shortest_path_seen)):
        path.append([ant_colony.shortest_path_seen[i], ant_colony.shortest_path_seen[(i+1) % len(ant_colony.shortest_path_seen)]])

    if iteration % 1 == 0:
        generate_plot(iteration, path, ant_colony.shortest_distance)
