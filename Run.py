import numpy as np
import math
import sys

from Mrowki import AntColony

class Miasto:

    def __init__(self, x=0, y=0, nr=1, odzwiedzony =0):
        self.x = x
        self.y=y
        self.nr = nr
        self.odwiedzony = odzwiedzony

    #liczenie odleglosci
    def odleglosc(self, a):
        odleglosc = (self.x - a.x) * (self.x - a.x) + (self.y - a.y) * (self.y - a.y)
        gotowa = math.sqrt(odleglosc)
        return gotowa


plik = (input("Podaj nazwe pliku")+".txt")
# ile jest wierzcholkow
with open(plik) as f:
    first_line = f.readline()
    pierwsza = first_line.split()
    print("Ilosc miast do odwiedzenia to", pierwsza[0], "\n")
ilosc = int(pierwsza[0])

miasta = []
for i in range(ilosc):
    miasta.append(Miasto())


for klapa in range(int(pierwsza[0])):

    #dodaje kazdy nr, x i y kazdego miasta do 3 list oraz sprawdza czy kazda z tych wartosci jest liczbowa
    with open(plik, 'r') as f:
        for i, x in enumerate(f):
            if i >= 1:
                Linia = x.split()
                p = Linia[0].isdigit()
                l = Linia[1].isdigit()
                k = Linia[2].isdigit()
                if p+l+k is 3 or 0 or 1 or 2:
                    miasta[i-1].nr=float(Linia[0])
                    miasta[i-1].x=float(Linia[1])
                    miasta[i-1].y=float(Linia[2])

                else:
                    print("Błąd w pliku wejsciowym")
                    sys.exit(0)

odleglosci = np.zeros((ilosc, ilosc))
for k in range(ilosc):
    for l in range(ilosc):
        dana = miasta[k].odleglosc(miasta[l])
        if (dana==0):
            odleglosci[k][l] = np.inf
        else:
            odleglosci[k][l] = dana
#print(odleglosci)
#ant_colony = AntColony(odleglosci, 100, 10, 500, 0.8699, alpha=1, beta=2)
ant_colony = AntColony(odleglosci, 100, 10, 500, 0.8699, alpha=1.35, beta=19.35)
najkrotsza = ant_colony.run()
print ("Najkrotsza trasa to: {}".format(najkrotsza))
