import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df = pd.read_csv('results/results.csv', decimal=',')
df = df.astype('int64')

df['d_inv'] = 1/df['d']
print(df)
X = 1/df['d'].values.reshape(-1, 1)  # values converts it into a numpy array
Y = df['c0'].values.reshape(-1, 1)  # -1 means that calculate the dimension of rows, but have 1 column

# plt.plot(1/df['d'], df['c0'], 'o')
plt.grid(True)
plt.xlabel("1/d [1/cm]")
plt.ylabel("C0 [pF]")

fig = plt.gcf()
fig.set_facecolor('white')
ax = plt.gca()

linear_regressor = LinearRegression()  # create object for the class
linear_regressor.fit(X, Y)  # perform linear regression
Y_pred = linear_regressor.predict(X)  # make predictions

plt.plot(1/df['d'], Y_pred, color='red')
plt.errorbar(1/df['d'], df['c0'], df['c0']*0,xerr=0.01, fmt='o')
plt.show()

